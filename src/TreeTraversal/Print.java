package TreeTraversal;

public class Print {
	
	public void display(Node root,Traversal traversal){
		System.out.println("Traverse with "+traversal+" : "+traversal.traverse(root));
	}
}
