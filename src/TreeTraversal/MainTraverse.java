package TreeTraversal;

public class MainTraverse {
	public static void main(String[] args){
		Node h = new Node("H",null,null);
		Node i = new Node("I",h,null);
		Node g = new Node("G",null,i);
		Node e = new Node("E",null,null);
		Node c = new Node("C",null,null);
		Node d = new Node("D",c,e);
		Node a = new Node("A",null,null);
		Node b = new Node("B",a,d);
		Node f = new Node("F",b,g);
		
		PreOrder preorder = new PreOrder();
		InOrder inorder = new InOrder();
		PostOrder postorder = new PostOrder();
		
		Print con = new Print();
		
		con.display(f, preorder);
		con.display(f, inorder);
		con.display(f, postorder);
		
	}
}
