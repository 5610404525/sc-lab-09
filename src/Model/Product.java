package Model;

import Interface.Taxable;

public class Product implements Taxable ,Comparable<Product> {
	private String name;
	private double price;

	public Product(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public String getName() {
		return this.name;
	}

	public double getPrice() {
		return this.price;
	}

	@Override
	public double getTax() {
		return 0.07 * this.getPrice();
	}

	@Override
	public int compareTo(Product o) {
		if(this.getPrice()==o.getPrice()){
			return -1;
		}
		else if (this.getPrice()<o.getPrice()){
			return 0;
			}
		else{
			return 1;
		}

	}	

}
