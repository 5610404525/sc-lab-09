package Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import Interface.Taxable;
import Model.Company;
import Model.CompareAll;
import Model.Person;
import Model.Product;

public class Test1 {

	public static void main(String[] args) {
		ArrayList<Taxable> listtax = new ArrayList<Taxable>();
		Taxable[] list = new Taxable[3];
		list[0] = new Person("x",158,100000);
		list[1] = new Product("y",8000);
		list[2] = new Company("z",35000,4000);
		for(int i = 0 ; i < list.length;i++){
			listtax.add(list[i]);
		}
		
		Collections.sort(listtax,new CompareAll());
		
		
	}

}
